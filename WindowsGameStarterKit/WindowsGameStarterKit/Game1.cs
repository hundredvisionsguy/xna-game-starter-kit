using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using XRpgLibrary;
using WindowsGameStarterKit.GameScreens;
namespace WindowsGameStarterKit
{
     public class Game1 : Microsoft.Xna.Framework.Game
     {
         #region XNA Field Region
         GraphicsDeviceManager graphics;
         public SpriteBatch SpriteBatch;
         #endregion 

         #region Game State Region
         GameStateManager stateManager;             // declare state manager
         public TitleScreen TitleScreen;            // declare title screen
         #endregion

         #region Screen Field Region
         const int screenWidth = 800;               // set screen width in pixels
         const int screenHeight = 600;              // set screen height in pixels
         public readonly Rectangle ScreenRectangle; // declare a screen rectangle
         #endregion
         public Game1()
         {
             graphics = new GraphicsDeviceManager(this);
             graphics.PreferredBackBufferWidth = screenWidth;   // apply screen width to graphics 
             graphics.PreferredBackBufferHeight = screenHeight; // apply screen height to graphics
             ScreenRectangle = new Rectangle(
                 0,
                 0,
                 screenWidth,
                 screenHeight);                                 // set dimensions on the screen size using a rectangle
             Content.RootDirectory = "Content";
             Components.Add(new InputHandler(this));            // add input handler
             stateManager = new GameStateManager(this);         // initialize game state manager
             Components.Add(stateManager);                      // add state manager to components
             TitleScreen = new TitleScreen(this, stateManager); // initialize a title screen 
             stateManager.ChangeState(TitleScreen);             // change state to the title screen
         }
         protected override void Initialize()
         {
            base.Initialize();
         }
         protected override void LoadContent()
         {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
         }
         protected override void UnloadContent()
         {
         }
         protected override void Update(GameTime gameTime)
         {
             if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
             this.Exit();
             base.Update(gameTime);
         }
         protected override void Draw(GameTime gameTime)
         {
             GraphicsDevice.Clear(Color.CornflowerBlue);
             base.Draw(gameTime);
         }
     }
}