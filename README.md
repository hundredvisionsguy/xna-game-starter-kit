XNAGameStarter
==============

A Basic XNA Starter Code for any 2-D game. This comes from a series of excellent tutorials by Jamie McMahon titled, XNA Game Programming Adventures
XNA 4.0 RPG Tutorials (http://xnagpa.net/xna4rpg.php).

I plan to work through the tutorials, adding the components, but commenting out or even leaving out some of the code for my students to complete and branch off of. My goal is to create something not unlike Game Maker, but using XNA (no drag & drop windows).
